# Projet Exploration Galactique

## Exercice 0

### Etape 1: 
- Creation du projet avec la librairie Create-react-app
- Reflexion sur le thème que l'on va presenter

### Etape 2:
- Creation d'un dockerfile
- Creation d'un dossier pour monter le volume
- Le volume se trouve dans src/data c'est pour changer d'image
- Pour construire localement l'image voici la commande  docker build -t registry.gitlab.com/alphabalde/project-ci-cd-ma .
- docker run --name galaxy-app-develop -d -v "$(pwd)/src/data:/usr/src/app/src/data" -p 3000:3000 registry.gitlab.com/alphabalde/project-ci-cd-ma

## Exercice 1

### Etape 1: Activation de AutoDevOps

- Création de la branche "auto-devops" dans le repository GitLab.
- Activation de AutoDevOps dans les paramètres de CI/CD du projet (voir "image_exo1_1" dans "project-ci-cd-ma\Exercices\Exercice_1\images").

### Etape 2: Mise en place des tests

- Création d'un dossier "tests" dans le projet.
- Ajout d'un fichier de test "test.global.js" dans le dossier "tests".
- Utilisation de Jest pour exécuter ces tests.

### Etape 3: Configuration du fichier .gitlab-ci.yml pour AutoDevOps

- Ajout de la configuration suivante pour intégrer AutoDevOps dans le fichier .gitlab-ci.yml :
  include:

  - template: Auto-DevOps.gitlab-ci.yml

- Choix de l'image Docker (docker:latest) pour le pipeline.
- Définition des variables nécessaires.
- Configuration du déploiement dans l'environnement de staging, dénommé 'develop' dans le projet.
  (voir "image_exo1_2" dans "project-ci-cd-ma\Exercices\Exercice_1\images")

### Explication de la configuration de la pipeline et des phases :

#### Explication de configuration

- La première étape est un test (test-1) qui consiste simplement à afficher "Test 1 réussi".

- La deuxième étape (job-1) est le déploiement.
- Elle utilise le service Docker-in-Docker (docker:dind) pour construire et déployer une image Docker.
- Avant de déployer, elle se connecte au registre Docker du projet en utilisant les variables d'environnement $CI_REGISTRY_USER et $CI_JOB_TOKEN.
- Elle supprime une éventuelle instance précédente du conteneur Docker (galaxy-app-develop).
- Elle définit la référence de l'image Docker (DOCKER_IMAGE_REF) en fonction de la branche actuelle.
- Elle affiche un message puis lance un conteneur Docker basé sur l'image construite, en mappant le port 3001 de l'hôte au port 3000 du conteneur.
- Cette étape représente le déploiement de l'application dans l'environnement de staging.

#### Explication des phases

La pipeline d'intégration et de déploiement continu (CI/CD) dans GitLab comprend les étapes suivantes :

Build : Cette étape construit l'application, préparant le code source pour le déploiement.

Test : Les tests sont exécutés pour garantir le bon fonctionnement de l'application.

Code Quality : Cette phase évalue la qualité du code source, identifiant les zones nécessitant des améliorations en termes de lisibilité et de maintenabilité.

Container Scanning : Cette étape analyse les images de conteneurs pour détecter d'éventuelles vulnérabilités ou problèmes de sécurité.

Node.js Scan SAST : Il s'agit d'une analyse de sécurité statique spécifique à Node.js, cherchant des vulnérabilités connues dans le code source.

Secret Detection : Identifie et signale la présence de secrets sensibles dans le code source pour renforcer la sécurité.

Semgrep SAST : Utilise Semgrep, un outil d'analyse statique du code, pour détecter des problèmes de sécurité et des erreurs de codage.

Test-1 : Une étape personnalisée définie dans votre fichier .gitlab-ci.yml, pour afficher un message indiquant que le "Test 1" a réussi.

Deploy : Cette phase déploie l'application dans un environnement de staging pour des tests plus approfondis avant le déploiement en production.

Job-1 : Cette étape supplémentaire utilise Docker pour gérer le déploiement de l'application dans un conteneur. Elle se connecte au registre Docker, supprime les instances précédentes, construit une nouvelle image Docker, puis lance le conteneur.

(voir "image_exo1_3" dans "project-ci-cd-ma\Exercices\Exercice_1\images")


## Exercice 2

### Etape 1
- Creation des environements de production et developpement
- Mise en place d'un déploiement pour la version de déveleppement sur vercel en suivant la documentation de vercel
  (voir "image_exo2_1" dans "project-ci-cd-ma\Exercices\Exercice_2\images")
- Mise en place d'un déploiement pour la version de production sur gitlab Pages en suivant la documentation de Gitlab

## Exercice 3
- Tout d'abord on a regardé la documentation de owsap et une vidéo youtube Ensuite on a mis en place le pipeline
- Le owsap va scanner tout le site pour trouver des vulnerabilité comme les failles xss etc. (voir "image_exo3_1)
- Dans le site que l'on a développé le rapport à trouver une faille clickjacking, c'est une attaque où un utilisateur est trompé pour cliquer sur du contenu malveillant.
- Les rapports se trouvent dans le job scan. On peut aussi les retrouver dans l'onglet artifacts.

