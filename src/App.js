import React from "react";
import GalaxyPage from "./components/templates/GalaxyPage";

const App = () => (
  <div>
    <GalaxyPage />
  </div>
);

export default App;
