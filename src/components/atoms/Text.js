import React from "react";

const Text = ({ content }) => <p className="text-lg my-2">{content}</p>;

export default Text;
