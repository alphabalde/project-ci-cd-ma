import React from "react";

const Title = ({ text }) => <h1 className="text-4xl font-bold my-4">{text}</h1>;

export default Title;
