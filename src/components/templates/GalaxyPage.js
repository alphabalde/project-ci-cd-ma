import React from "react";
import Footer from "../organisms/Footer";
import GalaxyInfo from "../organisms/GalaxyInfo";
import Header from "../organisms/Header";

const GalaxyPage = () => (
  <div>
    <Header />
    <GalaxyInfo />
    <Footer />
  </div>
);

export default GalaxyPage;
