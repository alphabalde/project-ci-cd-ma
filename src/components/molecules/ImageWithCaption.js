import React from "react";

const ImageWithCaption = (props) => {
  const { src, caption } = props;
  return (
    <div className="my-4" {...props}>
      <img src={src} alt={caption} className="rounded-md" />
      <p className="text-sm text-gray-600 mt-2">{caption}</p>
    </div>
  );
};

export default ImageWithCaption;
