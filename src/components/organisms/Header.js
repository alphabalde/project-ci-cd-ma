import React from "react";

const Header = () => (
  <header className="bg-black text-white p-4">
    <div className="container mx-auto">
      <h1 className="text-2xl font-bold">Exploration Galactique</h1>
    </div>
  </header>
);

export default Header;
