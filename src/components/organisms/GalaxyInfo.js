import React from "react";
import imageLinks from "../../data/imageLinks.json";
import Text from "../atoms/Text";
import Title from "../atoms/Title";
import ImageWithCaption from "../molecules/ImageWithCaption";

const GalaxyInfo = () => (
  <div className="max-w-4xl mx-auto p-4 flex flex-col items-center">
    <Title className="text-4xl mb-4" text="Exploration de la Galaxie" />
    <Text
      className="text-lg mb-4"
      content="Découvrez les mystères fascinants de notre galaxie, la Voie lactée. La Voie lactée est une gigantesque structure en forme de disque composée de milliards d'étoiles, de planètes, de gaz et de poussières interstellaires."
    />

    <Text
      className="text-lg mb-4"
      content="Notre galaxie abrite une diversité incroyable d'objets célestes, y compris des nébuleuses colorées, des étoiles massives, des trous noirs mystérieux et bien plus encore."
    />

    <div className="flex justify-between">
      {imageLinks.images.map((link, index) => (
        <ImageWithCaption
          key={index}
          src={link}
          caption={`Image ${index + 1}`}
          className="mx-2 mb-4"
        />
      ))}
    </div>

    <Text
      className="text-lg mb-4"
      content="Explorez nos galeries d'images pour admirer la beauté captivante des différentes régions de la Voie lactée. Chaque image offre un aperçu unique de notre cosmos."
    />

    <Text
      className="text-lg mb-4"
      content="N'hésitez pas à contacter notre équipe d'experts pour en savoir plus sur les découvertes récentes, les missions d'exploration spatiale et les avancées scientifiques liées à la Voie lactée."
    />

    <Text className="text-lg mb-4" content="Test AutoDevops" />
  </div>
);

export default GalaxyInfo;
