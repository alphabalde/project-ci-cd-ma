import React from "react";

const Footer = () => (
  <footer className="bg-black text-white p-4 mt-8">
    <div className="container mx-auto">
      <p className="text-sm">
        &copy; 2023 Exploration Galactique. Tous droits réservés.
      </p>
    </div>
  </footer>
);

export default Footer;
